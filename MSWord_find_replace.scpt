tell application "Microsoft Word"
	set myResult to find object of selection
	tell myResult
		execute find find text "" replace with "←" replace replace all
		execute find find text "" replace with "→" replace replace all
		execute find find text "" replace with "↑" replace replace all
		execute find find text "" replace with "↓" replace replace all
		execute find find text "" replace with "➞" replace replace all
	end tell	
end tell

tell application "Microsoft Word"
	activate
	run VB macro macro name "update_styles"
end tell